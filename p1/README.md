
# LIS4331 Advanced Mobile Applications Development

## Brian Kelly

### Project #1 Requirements:

1. Video of Media Player App running
2. Screenshot of Skillset 7
3. Screenshot of Skillset 8
4. Screenshot of Skillset 9

#### Assignment Requirements:

|*Video of Media Player App Running*|
|:---                               |
|[![Media Player App Running](img/videoThumbnail.png)](https://www.youtube.com/watch?v=uyIOjD4JgGg&ab_channel=King_Kellington1 "Media Player App")|

|*Skillset 7 Screenshot Pt. 1*|*Skillset 7 Screenshot Pt. 2*|
| :---                        | :---                        |
|![Skillset 7 Screenshot](img/lis4331SkillSet7pt1.png)|![Skillset 7 Screenshot](img/lis4331SkillSet7pt2.png)|

|*Skillset 8 GIF*|
| :---                        |
|![Skillset 8 GIF](https://media.giphy.com/media/YvFAlpYke4kvBvaNaX/giphy.gif)|

|*Skillset 9 GIF*|
| :---                        |
|![Skillset 9 GIF](https://media.giphy.com/media/8fqbgz7usrOYsCOJKj/giphy.gif)|
