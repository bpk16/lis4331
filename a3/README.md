


# LIS4331 Advanced Mobile Applications Development

## Brian Kelly

### Assignment #3 Requirements:

1. Create Currency Converter Application
2. Screenshot of running Currency Converter's Splash Screen
3. Screenshot of running Currency Converter's unpopulated user interface
4. Screenshot of running Currency Converter's toast notification
5. Screenshot of running Currency Converter's converted currency user interface
6. Screenshot of skillset 4
7. Screenshot of skillset 5
8. Screenshot of skillset 6

#### Assignment Screenshots:

|*Screenshot of Android Studio - Currency Converter App Unpopulated*|*Screenshot of Android Studio - Currency Converter App Populated*|
| :---                                         | :---                                                   |
|![Currency Converter App - Unpopulated](img/currencyConverterAppUnpopulated.PNG)|![Currency Converter App - Populated](img/currencyConverterAppPopulated.PNG)|

|*Screenshot of Android Studio - Currency Converter App Splash Screen*|*Screenshot of Android Studio - Currency Converter App Toast Notification*|
| :---                                         | :---                                                   |
|![Currency Converter App Splash Screen](img/currencyConverterAppSplashScreen.PNG)|![Currency Converter App Toast Notification](img/currencyConverterAppToastNotification.PNG)|

|*Screenshots of Skillset 4*|
| :---                     |
|![Skillset 4 Screenshots](img/lis4331SkillSet4.PNG)|

|*Screenshots of Skillset 5 Pt. 1*|*Screenshot of Skillset 5 Pt. 2*|*Screenshot of Skillset 5 Pt. 3*|
|:---                      |:---                                  |:---                            |
|![Skillset 5 Screenshots](img/lis4331SkillSet5Pt1.PNG)|![Skillset 5 Screenshots](img/lis4331SkillSet5Pt2.PNG)|![Skillset 5 Screenshots](img/lis4331SkillSet5Pt3.PNG)|

|*Screenshots of Skillset 5 Pt. 4*|*Screenshots of Skillset 5 Pt. 5*|
|:---                            |:---                            |
|![Skillset 5 Screenshots](img/lis4331SkillSet5Pt4.PNG)|![Skillset 5 Screenshot](img/lis4331SkillSet5Pt5.PNG)|

|*Screenshots of Skillset 6 Pt. 1*|*Screenshots of Skillset 6 Pt. 2*|*Screenshot of Skillset 6 Pt. 3*|
| :---                           |:---                            |:---                            |     
|![Skillset 6 Screenshots](img/lis4331SkillSet6Pt1.PNG)|![Skillset 6 Screenshots](img/lis4331SkillSet6Pt2.PNG)|![Skillset 6 Screenshots](img/lis4331SkillSet6Pt3.PNG)|

|*Screenshots of Skillset 6 Pt. 4*|*Screenshots of Skillset 6 Pt. 5*|*Screenshot of Skillset 6 Pt. 6*|
| :---                           |:---                            |:---                            |
|![Skillset 6 Screenshots](img/lis4331SkillSet6Pt4.PNG)|![Skillset 6 Screenshots](img/lis4331SkillSet6Pt5.PNG)|![Skillset 6 Screenshots](img/lis4331SkillSet6Pt6.PNG)|