> **NOTE:** This README.md file should be placed at the **root of each of your main directory.**

# LIS4331 Advanced Mobile Applications Development

## Brian Kelly

### LIS 4331 Requirements:

*Course Work Links (Assignments):*

1. [A1 README.md](a1/README.md "My A1 README.md file")
    - Install Java
    - Install Android Studio
    - Create MyFirstApp
    - Create Contacts App
    - Provide screenshots of installation
    - Create bitbucket repo
    - Complete bitbucket tutorial(bitbucketstationlocations)
    - Provide git command descriptions
2. [A2 README.md](a2/README.md "My A2 README.md file")
    - Create Tip Calculator App
    - Screenshot of running Tip Calculator App
    - Screenshot of skillset 1
    - Screenshot of skillset 2
    - Screenshot of skillset 3
3. [A3 README.md](a3/README.md "My A3 README.md file")
    - Create Currency Converter Application
    - Screenshot of running Currency Converter's Splash Screen
    - Screenshot of running Currency Converter's unpopulated user interface
    - Screenshot of running Currency Converter's toast notification
    - Screenshot of running Currency Converter's converted currency user interface
    - Screenshot of skillset 4
    - Screenshot of skillset 5
    - Screenshot of skillset 6
4. [A4 README.md](a4/README.md "My A4 README.md file")
    - Create Mortgage Rate Application
    - Screenshot of running Mortgage Rate App's Splash Screen
    - Screenshot of running Mortgage Rate App's unpopulated user interface
    - Screenshot of running Mortgage Rate App's invalid entry
    - Screenshot of running Mortgage Rate App's valid entry
    - Screenshot of skillset 10
    - Screenshot of skillset 11
    - Screenshot of skillset 12
5. [A5 README.md](a5/README.md "My A5 README.md file")
    - Screenshot of News Reader App Items Activity Screen
    - Screenshot of News Reader App Item Activity Screen
    - Screenshot of News Reader App Read More Screen
    - Screenshot of Skillset 13
    - Screenshot of Skillset 14
    - Screenshot of Skillset 15


*Course Work Links (Projects):*

1. [P1 README.md](p1/README.md "My P1 README.md file")
    - Video of Media Player App running
    - Screenshot of Skill Set 7
    - Screenshot of Skill Set 8
    - Screenshot of Skill Set 9
2. [P2 README.md](p2/README.md "My P2 README.md file")
    -  Gif of My User App running
    -  Pictures of My User App View Popup
    -  Pictures of My User App Add Popup
    -  Pictures of My User App Update Popup
    -  Pictures of My User App Delete Popup



