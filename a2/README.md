


# LIS4331 Advanced Mobile Applications Development

## Brian Kelly

### Assignment #2 Requirements:

1. Create Tip Calculator
2. Screenshot of running Tip Calculator App
3. Screenshot of skillset 1
4. Screenshot of skillset 2
5. screenshot of skillset 3

#### Assignment Screenshots:

|*Screenshot of Android Studio - Tip Calculator App Unpopulated*|*Screenshot of Android Studio - Tip Calculator App Populated*|
| :---                                         | :---                                                   |
|![Tip Calculator App - Unpopulated](img/tipCalculatorAppUnpopulated.PNG)|![Tip Calculator App - Populated](img/tipCalculatorAppPopulated.PNG)|

|*Screenshot of Skillset 1*|*Screenshot of Skillset 2*|
| :---                     | :---                     |
|![Skillset 1 Screenshot](img/lis4331Skillset1.PNG)|![Skillset 2 Screenshot](img/lis4331Skillset2.PNG)|

|*Screenshot of Skillset 3*|    |
| :---                                             | --- |
|![Skillset 3 Screenshot](img/lis4331Skillset3.PNG)|     |