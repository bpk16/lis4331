
# LIS4331 Advanced Mobile Applications Development

## Brian Kelly

### Project #2 Requirements:

1. Gif of My User App running
2. Pictures of My User App View Popup
3. Pictures of My User App Add Popup
4. Pictures of My User App Update Popup
5. Pictures of My User App Delete Popup

#### Assignment Requirements:

|*My Users App Gif*|
| :---             |
|![My Users App Gif](img/lis4331P2.gif)|

|*My Users App Splash Screen*|*My Users App Home Screen*|
| :---  | :---  |
|![My Users App Splash Screen](img/p2SplashScreen.png)|![My Users App Home Screen](img/p2HomeScreen.png)|

|*My Users App Add Screen*|*My Users App View Screen*|*My Users App Update Screen*|
| :---                        | :---                        | :---                |
|![My Users App Add Screen](img/p2AddScreen.png)|![My Users App View Screen](img/p2ViewScreen.PNG)|![My Users App Update Screen](img/p2UpdatedScreen.PNG)|

|*My Users App View Screen*|*My Users App Delete Screen*|*My Users App View Screen*|
| :---                        | :---                        | :---                |
|![My Users App View Screen](img/p2ViewScreenPt2.png)|![My Users App Delete Screen](img/p2DeleteScreen.png)|![My Users App View Screen](img/p2ViewScreenPt3.png)|