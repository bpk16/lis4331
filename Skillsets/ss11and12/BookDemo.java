import java.util.Scanner;
import java.text.DecimalFormat;

class BookDemo 
{
    public static void main(String[] args) 
    {
        String cd = "";
        String des = "";
        double pc = 0.0;
        String au = "";
        Scanner sc = new Scanner(System.in);

        DecimalFormat currency = new DecimalFormat("$##.##");

        System.out.println("Below are base-class default values");

        productClass p1 = new productClass();
        System.out.println("\nCode: " + p1.getCode());
        System.out.println("Description: " + p1.getDescription());
        System.out.println("Price: " + currency.format(p1.getPrice()));

        System.out.println("Below are base-class user entered values");

        System.out.print("Code: ");
        cd = sc.nextLine();

        System.out.print("Description: ");
        des = sc.nextLine();

        System.out.print("Price: ");
        pc = sc.nextDouble();

        productClass p2 = new productClass();

        System.out.println("\nCode: " + p2.getCode());
        System.out.println("Description: " + p2.getDescription());
        System.out.println("Price: " + currency.format(p2.getPrice()));

        System.out.println("*Below using setter values to pass literal values, then using print() method to display values*");
        p2.setCode("xyz789");
        p2.setDescription("Test Widget");
        p2.setPrice(89.99);
        p2.print();

        System.out.println();

        System.out.println("*Below are derived class user entered values*");
        Book b1 = new Book();
        System.out.print("Code: " + b1.getCode());
        System.out.print("Description: " + b1.getDescription());
        System.out.print("Price: " + currency.format(b1.getPrice()));
        System.out.print("Author: " + b1.getAuthor());

        System.out.println("Or\n");
        b1.print();

        System.out.println("*Below are derived class user entered values*");

        System.out.print("Code: ");
        cd = sc.next();

        System.out.print("Description: ");
        des = sc.nextLine();

        System.out.print("Price: ");
        pc = sc.nextDouble();

        System.out.print("Author: ");
        au = sc.next();

        Book b2 = new Book();
        System.out.println("Code: " + b2.getCode());
        System.out.println("Description: " + b2.getDescription());
        System.out.println("Price: " + currency.format(b2.getPrice()));
        System.out.println("Author: " + b2.getAuthor());

        System.out.println("Or\n");
        b2.print();

    }
}
