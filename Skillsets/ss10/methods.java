import java.util.Scanner;

public class methods 
{
    public static void getRequirements()
    {
        System.out.println("Program calculates approximate travel time.");
        System.out.println("Program prompts users to enter miles and miles per hour.");
        System.out.println("Accept decimal entries.");
        System.out.println("Prompt user to continue (only if user enters \"y\" or \"Y\").");
        System.out.println("*Must* perform data validation.");
        System.out.println("Hint: Use integer arithmatic and division and modulus operators to calculate hours and minutes");
        System.out.println("Display approximate travel time in hours and minutes");

        System.out.println();
    }
    public static double validateMilesDataType()
    {
        double miles = 0.0;
        Scanner sc = new Scanner(System.in);

        System.out.print("Enter miles: ");
        while(!sc.hasNextDouble())
        {
            System.out.print("Invalid input--miles must be a number\n");
            System.out.print("Enter miles: ");
            sc.next();
        }
        miles = sc.nextDouble();
        return miles;
    }
    public static double validateMilesRange(double miles)
    {
        while (miles <= 0 || miles > 3000)
        {
            System.out.println("Miles must be greater than 0, and less than 3000.\n");
            miles = validateMilesDataType();
        }
        return miles;
    }
    public static double validateMPHDataType()
    {
        double mph = 0.0;
        Scanner sc = new Scanner(System.in);

        System.out.print("Enter mph: ");
        while(!sc.hasNextDouble())
        {
            System.out.print("Invalid input--mph must be a number\n");
            System.out.print("Enter mph: ");
            sc.next();
        }
        mph = sc.nextDouble();
        return mph;
    }
    public static double validateMPHRange(double mph)
    {
        while (mph <= 0 || mph > 100)
        {
            System.out.println("Mph must be greater than 0, and less than 100.\n");
            mph = validateMPHDataType();
        }
        return mph;
    }
    public static void calculateTravelTime(double miles, double mph)
    {
        double hours = 0.0;
        int minutes = 0;
        int hoursInt = 0;
        int minutesRemainder = 0;

        hours = miles/mph;

        minutes = (int)(hours * 60);

        hoursInt = minutes / 60;
        minutesRemainder = minutes % 60;

        System.out.println("Estimated travel time: " + hoursInt + " hr(s) " + minutesRemainder + " Minutes.");
        
    }
}


        
        