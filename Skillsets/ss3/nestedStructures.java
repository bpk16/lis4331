import java.util.Scanner;

class nestedStructure 
{
    public static void main(String args[])
    {
        System.out.println("Author: Brian Kelly.");
        System.out.println("Program counts, totals, and averages total number of user-entered exam scores.");
        System.out.println("Please enter exam scores between 0 and 100, inclusive. \nEnter out of range numbers to end the program.");
        System.out.println("Must *only* permit numeric entries.");

        double total = 0.0;
        int count = 0;
        double score = 0.0;
        double average = 0.0;
        Scanner input = new Scanner(System.in);

        while(score>=0 && score <=100) 
        {
            System.out.print("Enter exam score: ");

            while(!input.hasNextDouble())
            {
                System.out.println("Not a valid number");
                input.next();
                System.out.println("Please try again. Enter exam score: ");
            }
            score = input.nextDouble();

            if (score>=0 && score <=100)
            {
                count = ++count;
                total = total+score;
            }
        }
        average = total/count;

        System.out.println("Count: " + count);
        System.out.println("Total: " + total);
        System.out.println("Average: " + average);
        
    }
}
