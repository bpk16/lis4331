import java.util.Scanner;

public class MeasurementConverter
{
    public static void main (String[]args)
    {
        System.out.println("Program converts inches to centimeters, meters, feet, yards, and miles");
        System.out.println("***Notes***:"
            + "\n1) Use integer for inches (must validate integer input)."
            + "\n2) Use printf() function to print (format values per below output)."
            + "\n3) Creaate Java \"constants\" for the following values:"
            + "\n\tINCHES_TO_CENTIMETERS,"
            + "\n\tINCHES_TO_METERS,"
            + "\n\tINCHES_TO_FOOT,"
            + "\n\tINCHES_TO_YARD,"
            + "\n\tFEET_TO_MILE\n");

        int inches = 0;
        double centimeters = 0.0;
        double meters = 0.0;
        double feet = 0.0;
        double yards = 0.0;
        double miles = 0.0;

        final double INCHES_TO_CENTIMETERS = 2.54;
        final double INCHES_TO_METERS = .0254;
        final double INCHES_TO_FOOT = 12;
        final double INCHES_TO_YARD = 36;
        final double FEET_TO_MILE = 5280;

        Scanner input = new Scanner(System.in);

        System.out.println("Please enter number of inches: ");
        while (!input.hasNextInt())
        {
            System.out.println("Not a valid integer!\n");
            input.next();
            System.out.println("Please enter number of inches: ");
        }
        inches =input.nextInt();

        centimeters = inches * INCHES_TO_CENTIMETERS;
        meters = inches * INCHES_TO_METERS;
        feet = inches * INCHES_TO_FOOT;
        yards = inches * INCHES_TO_YARD;
        miles = feet / FEET_TO_MILE;

        System.out.printf("%,d inch(es) equals\n\n%,.6f centimeter(s)\n%,.6f meter(s)\n%,.6f feet\n%,.6fyard(s)\n%,.8f mile(s)\n", inches, centimeters, meters, feet, yards, miles);



    }
}