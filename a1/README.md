

# LIS 4331

## Brian Kelly

### Assignment #1  Requirements:

*Completed Steps:*

1. Set up Distributed Version Control with Git and Bitbucket
2. Download and install JDK and Android Studio 
3. Create initial 2 Applications
4. Answer Chapter 1 & 2 questions

#### README.md file should include the following items:

* Screenshot of running JDK java Hello
* Screenshot of running Android Studio My First App
* Screenshot of running Android Studio Contacts App
* Git commands with short descriptions
* Bitbucket repo links

> #### Git commands w/ short descriptions:

1. git-init - Create an empty Git repository or reinitialize an existing one
2. git-status - Show the working tree status
3. git-add - Add file contents to the index
4. git-commit - Record changes to the repository
5. git-push - Update remote refs along with associated objects
6. git-pull - Fetch from and integrate with another repository or a local branch
7. git-config - Get and set repository or global options

#### Assignment Screenshots:

| *Screenshot of running java Hello*                |
| :---                                              |
|![JDK Installation Screenshot](img/jdk_install.png)|

| *Screenshot of Android Studio - My First App* |
| :---                                          |
|![Android Studio Installation Screenshot](img/android_myfirstapp.png)|

|*Screenshot of Android Studio - Contacts App Home Screen*|*Screenshot of Android Studio - Contacts App Contact Screen 1*|
| :---                                         | :---                                                   |
|![Android Studio Contacts Application](img/android_contactsapp_homescreen.png)|![Android Studio Contacts Application](img/android_contactsapp_contact1.png)|




#### Tutorial Links:

*Bitbucket Tutorial - Station Locations:*
[A1 Bitbucket Station Locations Tutorial Link](https://bitbucket.org/bpk16/bitbucketstationlocations/src/master/ "Bitbucket Station Locations")

