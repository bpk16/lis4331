


# LIS4331 Advanced Mobile Applications Development

## Brian Kelly

### Assignment #4 Requirements:

1. Create Mortgage Rate Application
2. Screenshot of running Mortgage Rate App's Splash Screen
3. Screenshot of running Mortgage Rate App's unpopulated user interface
4. Screenshot of running Mortgage Rate App's invalid entry
5. Screenshot of running Mortgage Rate App's valid entry
6. Screenshot of skillset 10
7. Screenshot of skillset 11
8. Screenshot of skillset 12

#### Assignment Screenshots:

|*Screenshot of Android Studio - Mortgage Rate App's Splash Screen*|*Screenshot of Android Studio - Mortgage Rate App's Unpopulated User Interface*|
| :---                                         | :---                                                   |
|![Mortgage Rate App - Splash Screen](img/mortgageRateSplash.png)|![Mortgage Rate App - Unpopulated Screen](img/mortgageRateUnpopulated.png)|

|*Screenshot of Android Studio - Mortgage Rate App's Invalid Entry Screen*|*Screenshot of Android Studio - Mortgage Rate App's Valid Entry Screen*|
| :---                                         | :---                                                   |
|![Mortgage Rate App - Invalid Screen](img/mortgageRateFail.png)|![Mortgage Rate App - Valid Screen](img/mortgageRateSuccess.png)|

|*Screenshots of Skillset 10*|*Screenshots of Skillset 11*|
| :---                           | :---                           | 
|![Skillset 10 Screenshot](img/lis4331SkillSet10.png)|![Skillset 11 Screenshot](img/lis4331SkillSet11.png)|
 

|*Screenshots of Skillset 12*|
| :---                           |     
|![Skillset 12 Screenshot](img/lis4331SkillSet12.png)|

